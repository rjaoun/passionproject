﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;


namespace PassionProject_N01329942.Models
{
    public class Team
    {
        [Key,ScaffoldColumn(false)]
        public int TeamID { get; set; }

        [Required, StringLength(50), Display(Name ="Team Name")]
        public string TeamName { get; set; }

        [Required, StringLength(50), Display(Name = "Date Established")]
        public string EstablishDate { get; set; }

        // one team has many players one to many relationship
        public virtual ICollection<Player> PLayers { get; set; }

        // foreign key countryID
        public virtual Country country { get; set; }
    }
}