﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionProject_N01329942.Models.ViewModels
{
    public class PlayerEdit
    {
        public PlayerEdit()
        {

        }
        public virtual Player player { get; set; }

        public IEnumerable<Team> teams { get; set; }

        public IEnumerable<Country> countries { get; set; }
    }
}