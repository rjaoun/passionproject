﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PassionProject_N01329942.Models
{
    //This model will create three tables (Countries, Teams, Players)
    public class SoccerCMSContext : DbContext
    {
        public SoccerCMSContext()
        {

        }

        public DbSet<Country> Countries { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Player> Players { get; set; }
    }
}