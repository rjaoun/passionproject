﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace PassionProject_N01329942.Models
{
    public class Player
    {
        //One team has many players
        //One country has many players
        [Key, ScaffoldColumn(false)]
        public int PlayerID { get; set; }

        // Player First Name Column
        [Required, StringLength(50), Display(Name = "Player First Name")]
        public string PlayerFName { get; set; }

        // Player Last Name Column
        [Required, StringLength(50), Display(Name = "Player Last Name")]
        public string PlayerLName { get; set; }

        // Player Age Column
        [Required, StringLength(50), Display(Name = "Player Age")]
        public string PlayerAge { get; set; }

        // Player nationality Column
        [StringLength(50), Display(Name = "Nationality")]
        public string PlayerNationality { get; set; }

        // Player Position Column
        [StringLength(50), Display(Name = "Player Position")]
        public string PlayerPosition { get; set; }

        // one team can have many players
        public virtual Team team { get; set; }
    }

}