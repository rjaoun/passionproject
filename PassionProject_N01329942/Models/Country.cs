﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace PassionProject_N01329942.Models
{
    public class Country
    {
        //field for Country ID
        [Key]
        public int CountryID { get; set; }
        //field for Country Name
        [Required, StringLength(20), Display(Name = "Country Name")]
        public string CountryName { get; set; }
        //field for World Cup Participation
        [Required, StringLength(30), Display(Name = "World Cup Participation")]
        public string WCparticipate { get; set; }

        //field for World Cup Participation
        [Required, StringLength(30), Display(Name = "Number of World cups")]
        public string WCnumber { get; set; }

        // one Country has many teams.
        public virtual ICollection<Team> Teams { get; set; }
    }
}