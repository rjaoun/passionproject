﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using PassionProject_N01329942.Models;
using PassionProject_N01329942.Models.ViewModels;
using System.Diagnostics;
using System.IO;

namespace PassionProject_N01329942.Controllers
{
    public class PlayerController : Controller
    {
        private SoccerCMSContext db = new SoccerCMSContext();
        // GET: Player
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public ActionResult List()
        {
            /*
            string query = "select * from Players";
            IEnumerable<Player> players = db.Players.SqlQuery(query);
            */
            
             IEnumerable<Player> players = db.Players.ToList();
             

            return View(players);
        }
        public ActionResult Add()
        {
            PlayerEdit EditedPlayer = new PlayerEdit();
            EditedPlayer.teams = db.Teams.ToList();
            return View(EditedPlayer);
        }
        [HttpPost]
        public ActionResult Create(string PlayerFname_New, string PlayerLname_New,string PlayerAge_New, string PlayerNationality_New, string PlayerPosition_New, int TeamName_New)
        {
            string subquery = "insert into Players (PlayerFName, PlayerLName, PlayerAge,PlayerNationality, PlayerPosition, team_TeamID) " +
                           "values (@firstname,@lastname,@age,@nationality,@position,@teamname)";

            SqlParameter[] myparams = new SqlParameter[6];
            myparams[0] = new SqlParameter("@firstname", PlayerFname_New);
            myparams[1] = new SqlParameter("@lastname", PlayerLname_New);
            myparams[2] = new SqlParameter("@age", PlayerAge_New);
            myparams[3] = new SqlParameter("@nationality", PlayerNationality_New);
            myparams[4] = new SqlParameter("@position", PlayerPosition_New);
            myparams[5] = new SqlParameter("@teamname", TeamName_New);

            db.Database.ExecuteSqlCommand(subquery, myparams);
            Debug.WriteLine(subquery);

            return RedirectToAction("List"); // after adding a team, it will redirect back to List View.
        }

        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {

            PlayerEdit EditedPlayer = new PlayerEdit();
            EditedPlayer.teams = db.Teams.ToList();

            EditedPlayer.player = db.Players.Find(id);

            return View(EditedPlayer);
        }

        [HttpPost]
        public ActionResult Edit(int? id, string PlayerFName_Edit, string PlayerLName_Edit,string PlayerAge_Edit,string PlayerNationality_Edit,string PlayerPosition_Edit, int TeamID_Edit)
        {
            if ((id == null) || (db.Teams.Find(id) == null))
            {
                return HttpNotFound();
            }
            string subquery = "UPDATE players SET PlayerFName = @playerFname, PlayerLName = @playerLname, " +
                              "PlayerAge= @playerage, PlayerNationality= @playernationality, PlayerPosition= @playerposition," +
                              " team_TeamID =@team " +
                              " WHERE playerid = @id";
            SqlParameter[] myparams = new SqlParameter[7];
            myparams[0] = new SqlParameter("@playerFname", PlayerFName_Edit);
            myparams[1] = new SqlParameter("@playerLname", PlayerLName_Edit);
            myparams[2] = new SqlParameter("@playerage", PlayerAge_Edit);
            myparams[3] = new SqlParameter("@playernationality", PlayerNationality_Edit);
            myparams[4] = new SqlParameter("@playerposition", PlayerPosition_Edit);
            myparams[5] = new SqlParameter("@team", TeamID_Edit);
            myparams[6] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(subquery, myparams);

            return RedirectToAction("List");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }

            return View(player);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Player player = db.Players.Find(id);
            db.Players.Remove(player);
            db.SaveChanges();
            return RedirectToAction("Index"); // To go back to Country/Index 

        }
    }
}