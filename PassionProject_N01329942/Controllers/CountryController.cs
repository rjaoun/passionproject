﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using PassionProject_N01329942.Models;
using System.Diagnostics;
using System.IO;

namespace PassionProject_N01329942.Controllers
{
    public class CountryController : Controller
    {
        // to create a database which holds all the tables.
        private SoccerCMSContext db = new SoccerCMSContext();


        /* GET: Country/Index which is an action controller that gets the values 
           of Countries table from Country.cs Model and display them in application 
           using Index.cshtml View. 
        */
        public ActionResult Index()
        {
            return View(db.Countries.ToList());
        }

        public ActionResult New()
        {
            return View(db.Countries.ToList());
        }

        public ActionResult Add()
        {
            return View();
        }

        // Country/Add
        [HttpPost]
        public ActionResult Create(string CountryName_New, string WCparticipate_New, string WCnumber_New)
        {
            string subquery = "insert into Countries (CountryName, WCparticipate, WCnumber) " +
                           "values (@name,@participate,@wcnumber)";

            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@name", CountryName_New);
            myparams[1] = new SqlParameter("@participate", WCparticipate_New);
            myparams[2] = new SqlParameter("@wcnumber", WCnumber_New);

            db.Database.ExecuteSqlCommand(subquery, myparams);
            Debug.WriteLine(subquery);

            return RedirectToAction("Index");
        }

        /* Display the details of the selected row. 
        */
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Country country = db.Countries.Find(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }
        
        /* Edit Action will redirect to Country/Edit/CountryID=(CountryID) and displays 
         * all the information of the selected row using Edit.cshtml View.
        */
        [HttpPost]
        public ActionResult Edit(string CountryName_Edit, string WCparticipate_Edit, string WCnumber_Edit, int CountryID_Edit)
        {
            string query = "update Countries " +
                           " SET CountryName = @name, WCparticipate = @participate, WCnumber = @wcnumber " +
                           " where CountryID = @countryid ";

            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@name", CountryName_Edit);
            myparams[1] = new SqlParameter("@participate", WCparticipate_Edit);
            myparams[2] = new SqlParameter("@wcnumber", WCnumber_Edit);
            myparams[3] = new SqlParameter("@countryid", CountryID_Edit);

            db.Database.ExecuteSqlCommand(query, myparams);
            Debug.WriteLine(query);

            return RedirectToAction("Index");
        }

        /* Edit action to check if the URL contains an ID, if not then it will
         * give an error with the URL showing Country/Edit/ is not accessible
         * since it is missing  CountryID.
         */
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Country country = db.Countries.Find(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        /* Delete Action will redirect to Country/Delete/CountryID=3 for example.
         * and will ask the user if he/she wants to delete the selected data. If yes, 
         * then it will be deleted and redirected back to Country/List.
        */
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Country country = db.Countries.Find(id);
            if (country == null)
            {
                return HttpNotFound();
            }

            return View(country);
        }

        /* This action is to delete a country from the Country Table after pressing submit button
         and return to Country/Index
        */ 
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Country country = db.Countries.Find(id);
            db.Countries.Remove(country);
            db.SaveChanges();
            return RedirectToAction("Index"); // To go back to Country/Index 

        }
            
    }
}