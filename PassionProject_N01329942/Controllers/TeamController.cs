﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using PassionProject_N01329942.Models;
using PassionProject_N01329942.Models.ViewModels;
using System.Diagnostics;
using System.IO;

namespace PassionProject_N01329942.Controllers
{
    public class TeamController : Controller
    {
        private SoccerCMSContext db = new SoccerCMSContext();
        // GET: Team
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        /* List action controller to list the Teams table. Also,
        we will have one column of country name from Countries table
        */
        public ActionResult List()
        {
            
            IEnumerable<Team> teams = db.Teams.ToList();

            /* (Or we could use this way to display Team database content)
             * string query = "select * from Teams";
             * IEnumerable<Team> teams = db.Teams.SqlQuery(query); 
             */

            return View(teams);
        }

        // Add action controller to add a new team
        public ActionResult Add()
        {
            TeamEdit EditedTeam = new TeamEdit();
            EditedTeam.countries = db.Countries.ToList();
            return View(EditedTeam);
        }
        [HttpPost]
        public ActionResult Create(string TeamName_New, string EstablishDate_New, int CountryID_New)
        {
            string subquery = "insert into Teams (TeamName, EstablishDate, Country_CountryID) " +
                           "values (@name,@establishdate,@country)";

            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@name", TeamName_New);
            myparams[1] = new SqlParameter("@establishdate", EstablishDate_New);
            myparams[2] = new SqlParameter("@country", CountryID_New);

            db.Database.ExecuteSqlCommand(subquery, myparams);
            Debug.WriteLine(subquery);

            return RedirectToAction("List"); // after adding a team, it will redirect back to List View.
        }
        public ActionResult Edit(int? id)
        {
            TeamEdit EditedTeam = new TeamEdit();
            EditedTeam.countries = db.Countries.ToList();
            EditedTeam.team = db.Teams.Find(id);

            return View();
        }

        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        /*
         * I had to add [HttpGet] since I have two actions with the same name,
         * it gave me an error of ambigious actions. Therefore, I added [HttpGet]
         * which i got this help from 
         * https://forums.asp.net/t/1714974.aspx?+foreach+var+item+in+Model+Object+reference+not+set+to+an+instance+of+an+object+
         */
        [HttpGet]
        public ActionResult Edit(int id)
        {

            TeamEdit EditedTeam = new TeamEdit();
            EditedTeam.countries = db.Countries.ToList();
            
            EditedTeam.team = db.Teams.Find(id);

            return View(EditedTeam);
        }

        [HttpPost]
        public ActionResult Edit(int id,string TeamName_Edit, string EstablishDate_Edit, int CountryID_Edit)
        {
            if ((id == null) || (db.Teams.Find(id) == null))
            {
                return HttpNotFound();
            }
            string subquery = "UPDATE teams SET TeamName = @teamname, EstablishDate = @establishdate," +
                              " country_CountryID =@country " +
                              " WHERE teamid = @id";
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@teamname", TeamName_Edit);
            myparams[1] = new SqlParameter("@establishdate", EstablishDate_Edit);
            myparams[2] = new SqlParameter("@country", CountryID_Edit);
            myparams[3] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(subquery, myparams);

            return RedirectToAction("List");
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }

            return View(team);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Team team = db.Teams.Find(id);
            db.Teams.Remove(team);
            db.SaveChanges();
            return RedirectToAction("Index"); // To go back to Country/Index 

        }
    }
}