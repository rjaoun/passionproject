﻿using System.Web;
using System.Web.Mvc;

namespace PassionProject_N01329942
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
